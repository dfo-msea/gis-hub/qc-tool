import os
import glob
from osgeo import gdal
import logging


def list_tiffs(input_dir):
    """
    Get a list of TIFF filepaths.
    :param input_dir: input directory.
    :return: list of file names for tiffs.
    """
    os.chdir(input_dir)
    logging.info(f"Working directory: {input_dir}")
    logging.info("Getting tif filepaths...")
    files = [file for file in glob.glob("*.tif")]
    logging.info(f"Number of raster layers in {input_dir}: {len(files)}")
    return files


def check_integrity(file_list):
    for f in file_list:
        logging.info(">"*20 + f"  Checking integrity of: {f}  " + "<"*20)
        ds = gdal.Open(f)
        for i in range(ds.RasterCount):
            logging.info(ds.GetRasterBand(1).Checksum())
            if gdal.GetLastErrorType() != 0:
                logging.error(gdal.GetLastErrorMsg())


def main():
    # CLI arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", type=str, help="full path to input directory where raster files are located")
    args = parser.parse_args()

    # set up logging
    logging.basicConfig(filename=os.path.join(args.input_dir, "raster-checksum.log"),
                        filemode="w",
                        format="%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s",
                        level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logging.getLogger("").addHandler(console)


    # get list of tif paths
    tif_files = list_tiffs(args.input_dir)
    check_integrity(tif_files)


if __name__ == "__main__":
    main()
