# Title of project

__Main author:__  Michael Peterman  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: mpetermangis@gmail.com | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Quality control toolbox to be used for layers uploaded to the GIS Hub.


## Summary
ArcGIS Toolbox that prints messages to user while executing quality control checks such as repair geometry. Separate python script to check integrity of GeoTIFF files by attempting to calculate a Checksum for single-band raster datasets.


## Status
Completed


## Contents
1. zip file with internal README file for installing and using the tool.
2. raster_checksum.py for checking integrity of GeoTIFF files.


## Methods
1. __QualityReportTool.zip__ arcsupport.py library contains three classes with high-level functions that are
inconvenient to use or not available in ESRI's arcpy module.

2. __raster_checksum.py__ uses python GDAL wrapper and the Checksum() function on a raster band to check that it's valid. Logs information including any files that fail integrity check.


## Requirements
1. ArcPy module and ESRI software to run the tool.
2. raster_checksum requires GDAL’s and OGR’s Python bindings.


## Caveats
1. Sometimes the repair geometry tool in ArcGIS fails. Testing shows that QGIS may successfully repair geometry where ArcGIS fails.
2. Testing integrity of GeoTIFF files using raster_checksum.py only logs when no Checksum can be calculated for a raster dataset (assuming single-band).


## Uncertainty
NA


## Acknowledgements
* Genkimaps: https://github.com/genkimaps
* https://lists.osgeo.org/pipermail/gdal-dev/2013-November/037520.html

## References
NA
